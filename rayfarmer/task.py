from abc import ABC, abstractmethod
import os
from typing import List, Dict


class BaseTaskList(ABC):

    def __init__(self, config: Dict) -> None:
        """
        Setup base task list attributes

        Args:
            config: yaml config parsed into a dictionary
        """
        self.config = config

        # check for valid config
        assert self.config

    @abstractmethod
    def read_jobs(self, jobs: str) -> None:
        """
        Read the list of jobs from the txt file

        Args:
            jobs: path to the txt file containing the jobs
        """
        raise NotImplementedError("Base method not implemented")

    @abstractmethod
    def get_jobs(self) -> List[List[str]]:
        """
        Construct the list of bash commands to execute and return them

        Returns:
            list of bash commands to execute
        """
        raise NotImplementedError("Base method not implemented")


class TaskList(BaseTaskList):

    def __init__(self, config: Dict) -> None:
        """
        Setup basic task list attributes.
        The commands are executed directly in bash (no image).

        Args:
            config: yaml config parsed into a dictionary
        """
        super().__init__(config)

        # check for valid config
        assert "setup" in self.config
        assert type(self.config["setup"]) == list

    def read_jobs(self, jobs: str) -> None:

        # check if file exists
        assert os.path.isfile(jobs)
        self.jobs = []

        with open(jobs, "r") as f:
            for job in f:
                if job.startswith("{"):
                    assert "cmd" in job
                    assert "job_name" in job
                    self.jobs += [eval(job)]
                else:
                    self.jobs += [job]

    def get_jobs(self) -> List[List[str]]:

        # combine jobs with any setup commands
        jobs = []
        for i, job in enumerate(self.jobs):
            if type(job) == dict:
                jobs += [[job["job_name"],
                          ";".join(self.config["setup"] + [job["cmd"]])]]
            else:
                jobs += [[f"job{i}", ";".join(self.config["setup"] + [job])]]
        return jobs


class ALRBImageTaskList(TaskList):

    def __init__(self, config: Dict) -> None:
        """
        TaskList for executing jobs from an image using the
        setupATLAS functionality

        Args:
            config: yaml config parsed into a dictionary
        """
        super().__init__(config)

        # check for valid config
        assert "image" in self.config
        assert "image_setup" in self.config
        assert type(self.config["image_setup"]) == list

    def get_jobs(self) -> List[List[str]]:

        # setup
        setup = ";".join(self.config["setup"])

        # image
        image = self.config["image"]

        # add the setupATLAS command
        cmd = f"setupATLAS -c {image} -r \""

        # add the image setup
        cmd += ";".join(self.config["image_setup"])

        # stitch setup and image commands
        cmd = ";".join([setup, cmd])

        # add the individual commands
        jobs = []
        for i, job in enumerate(self.jobs):
            if type(job) == dict:
                jobs += [[job['job_name'], f"{cmd}; {job['cmd'].strip()}\""]]
            else:
                jobs += [[f"job{i}", f"{cmd}; {job.strip()}\""]]

        return jobs
