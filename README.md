# RayTaskFarmer

## Running on Cori

### Install with conda

```
module load python
conda create --name raytaskfarmer python=3.7
source activate raytaskfarmer
git clone git+ssh://git@gitlab.cern.ch:7999/berkeleylab/raytaskfarmer.git
pip install -e raytaskfarmer
```

### Running a simple ray test in an interactive multi-node job

Start an interactive allocation with `salloc` and run the ray test:

```
salloc -N 5 -C haswell -q interactive -t 01:00:00
source start-ray-cluster
ray-test
```

A successful job will end with a print out similar to this:

```
...
nodes connected to the cluster: {'10.128.0.37', '10.128.0.40', '10.128.0.36', '10.128.0.39', '10.128.0.38'}
ip addresses of created ray actors: ['10.128.0.39', '10.128.0.36', '10.128.0.37', '10.128.0.40', '10.128.0.38']
```

Exit the allocation after running the test with `exit`. Make sure with `uname -n` that you are back to login nodes.

### Run a RayTaskFarmer example in a batch job

Simply execute:

```
sbatch test_slurm_cluster.sbatch
```

This will run a test task on 5 Haswell nodes (with 32 CPU each) executing the jobs from the `test/test_tasks.txt` file.
