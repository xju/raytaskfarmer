#!/usr/bin/env python

from setuptools import find_packages
import pathlib
import setuptools


# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

setuptools.setup(
    name='RayTaskFarmer',
    version='0.0.1',
    description='Simple task farmer using ray to run multi-node jobs.',
    long_description=README,
    long_description_content_type='text/markdown',
    url='https://gitlab.cern.ch/berkeleylab/raytaskfarmer',
    packages=find_packages(),
    scripts=[
        'example/charmpp.sbatch',
        'example/test_slurm_cluster.sbatch',
        'raytaskfarmer.py',
        'scripts/ray-sync',
        'scripts/ray-test',
        'scripts/start-ray-cluster',
    ],
    install_requires=[
        "PyYAML",
        "ray==2.6.2",
        "setuptools",
    ],
    package_data={'config': ['config/*.yaml'],
                  'data': ['test/*.txt']},
)
